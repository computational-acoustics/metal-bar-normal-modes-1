# Metal Bar Normal Modes 1

ElmerFEM model for the search of normal modes of a metal bar.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Elastic Modes of a Metal Bar](https://computational-acoustics.gitlab.io/website/posts/6-elastic-modes-of-a-metal-bar/).
* [Refining the Metal Bar Model](https://computational-acoustics.gitlab.io/website/posts/7-refining-the-metal-bar-model/).

## Study Summary

The main parameters of the study are reported below.

### Material

|Parameter Name   | Symbol     | Value             | Unit   |
|-----------------|------------|-------------------|--------|
| Young's Modulus | $`Y`$      | $`70 \cdot 10^9`$ | Pascal |
| Poisson Ratio   | $`\kappa`$ | 0.35              | -      |


### Domain

| Shape              | Size                      | Mesh Algorithm                           | Mesh Parameters         | Element Order |
|--------------------|---------------------------|------------------------------------------|-------------------------|---------------|
| Square based prism | 10 X 10 X 100 millimetres | Quadrangle Mapping + Wire Discretisation | 20 Equidistant Segments | Linear        |

## Software Overview

The table below reports the software used for this project.

| Software                                           | Usage                |
|----------------------------------------------------|----------------------|
| [FreeCAD](https://www.freecadweb.org/)             | 3D Modeller          |
| [Salome Platform](https://www.salome-platform.org) | Pre-processing       |
| [ElmerFEM](http://www.elmerfem.org)                | Multiphysical solver |
| [ParaView](https://www.paraview.org/)              | Post-processing      |

## Repo Structure

* `elmerfem` contains the ElmreFEM project.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `meshing.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. The mesh is exported into `elmerfem` as `elmerfem/Mesh_1.unv`.

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

